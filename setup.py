from setuptools import setup, find_packages
import sys, os

VERSION = '0.1.13'

setup(name='garmire_download_ncbi_sra',
      version=VERSION,
      description="download and process SRA files",
      long_description="""""",
      classifiers=[],
      keywords='ncbi download rna seq',
      author='o_poirion',
      author_email='opoirion@hawaii.edu',
      url='',
      license='MIT',
      packages=find_packages(exclude=['examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      )
